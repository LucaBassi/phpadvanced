<?php

ob_start();
?>

    <h2>Login</h2>
<?php if (@$_GET['loginError'] == true) :?>
    <h5><span style="color:red">Login refusé</span></h5>
<?php endif ?>
    <article>
        <form class='form' method='POST' action="../index.php?action=login">
            <div class="container">
                <label for="userEmail"><b>Username</b>
                <input type="email" placeholder="Enter email address" name="inputUserEmailAddress" required>
                </label>
                <label for="userPsw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="inputUserPsw" required>
            </div>
            <div class="container">
                <button type="submit" class="btn btn-default">Login</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </form>
        <div class="container signin">
            <p>Besoin d'un compte <a href="../index.php?action=register">Register</a>.</p>
        </div>
    </article>





<?php
$contenu = ob_get_clean();
require "gabarit.php";
