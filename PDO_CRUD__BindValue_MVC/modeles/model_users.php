<?php


function checkLogin($userEmailAddress, $userPsw)
{
    $result = false;

    $params = array(':email' => $userEmailAddress);

    $loginQuery = "SELECT password FROM account WHERE email =  :email ";

    require_once 'modeles/connector.php';
    $queryResult = executeQuerySelect($loginQuery, $params);

    if (count($queryResult) == 1) {
        $userHashPsw = $queryResult[0]->password;
       // $hashPasswordDebug = password_hash($userPsw, PASSWORD_ARGON2I);
        $result = password_verify($userPsw, $userHashPsw);
    }

    return $result;


}


function registerNewAccount($username, $userEmailAddress, $userPsw)
{
    $result = false;

    //  $strSeparator = '\'';

    $userHashPsw = password_hash($userPsw, PASSWORD_ARGON2I);
    $params = array(
        ':username' => $username,
        ':email' => $userEmailAddress,
        ':password' => $userHashPsw
    );

    $registerQuery = 'INSERT INTO account (username,email,password) VALUES (:username , :email , :password )';


    require_once 'modeles/connector.php';
    $queryResult = executeQueryInsert($registerQuery,$params);

    if ($queryResult == true) {
        $result = $queryResult;
    }
    return $result;
}


?>