<?php

function executeQuerySelect($query, $params)

{
    $queryResult = null;

    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null) {

        $stmt = $dbConnexion->prepare($query);
        foreach ($params as $key => &$val) {

            $stmt->bindValue($key, $val, \PDO::PARAM_STR);
        }

        $stmt->execute();
        $queryResult = $stmt->fetchAll(PDO::FETCH_OBJ);

    }
    $dbConnexion = null;//close database connexion
//    return $queryResult;
    return $queryResult;
}

/**
 * This function is designed to insert value in database
 * @param $query
 * @param $params
 * @return bool|null : $statement->execute() return true is the insert was successful
 */


function executeQueryInsert($query, $params)
{
    $queryResult = null;
    $rowChecker = null;
    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null) {

        $stmt = $dbConnexion->prepare($query);
        foreach ($params as $key => &$val) {
            $stmt->bindValue($key, $val, \PDO::PARAM_STR);
        }

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $rowChecker = true;
        }
    }
    $dbConnexion = null;//close database connexion
    if ($rowChecker == true) {
        $queryResult = true;
    }
    return $queryResult;
}

function openDBConnexion()
{
    $tempDbConnexion = null;

    $sqlDriver = 'mysql';
    $hostname = 'localhost';
    $port = 3306;
    $charset = 'utf8';
    $dbName = 'mydemo';
    $userName = 'root';
    $userPwd = '';
    $dsn = $sqlDriver . ':host=' . $hostname . ';dbname=' . $dbName . ';port=' . $port . ';charset=' . $charset;

    try {
        $tempDbConnexion = new PDO($dsn, $userName, $userPwd);
    } catch (PDOException $exception) {
        echo 'Connection failed: ' . $exception->getMessage();
    }
    return $tempDbConnexion;
}
